export interface ProductModelServer {
    id: number;
    name: string;
    category: string;
    price: number;
    image: string;
    description: string;
    quantity: number;
    images: string;
}

export interface ServerResponse {
    count: number;
    products: ProductModelServer[];
}